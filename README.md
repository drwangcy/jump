### MCU
* 华为实时操作系统LiteOS学习: https://gitee.com/thin-wind/Study/tree/LiteOS
* LwIP网络协议栈学习: https://gitee.com/thin-wind/Study/tree/LwIP
* HarmonyOS设备开发: https://gitee.com/thin-wind/HarmonyOS/tree/Mini-System
* 软件控制IO口模拟I2C时序: https://gitee.com/thin-wind/Module/tree/SwI2C
* 如何使用cJSON: https://gitee.com/thin-wind/Module/tree/cJSON
* 循环缓冲器: https://gitee.com/thin-wind/Module/tree/cir_buff
* 软件定时器: https://gitee.com/thin-wind/Module/tree/SwTimer
* 自实现的一种硬件抽象层: https://gitee.com/thin-wind/Module/tree/HAL
* 制作升级包: https://gitee.com/thin-wind/pybin
* FatFs文件系统移植与使用: https://gitee.com/thin-wind/fatfs
* LVGL图形库移植与使用: https://gitee.com/thin-wind/lvgl
* lwIP网络协议栈移植与使用: https://gitee.com/thin-wind/lwip

### 驱动开发
* ARM可信固件(TF-A)移植: https://gitee.com/thin-wind/tf-a
* 跟我一起写Makefile: https://gitee.com/thin-wind/Study2/tree/Makefile
* 从实战出发，深入解读uboot: https://gitee.com/thin-wind/uboot
* linux移植、驱动等学习: https://gitee.com/thin-wind/Study/tree/kernel

### 操作系统
* 如何从零实现一个RTOS: https://gitee.com/thin-wind/NK-RTOS
* 8086汇编基础: https://gitee.com/thin-wind/Study2/tree/asm-8086
* Linux0.12源码完全解读: https://gitee.com/thin-wind/Linux-0.12
* 如何从零实现一个操作系统: https://gitee.com/thin-wind/KOS

### 网络/应用编程
* python实现的小工具: https://gitee.com/thin-wind/python
* 《UNIX环境高级编程》随手代码: https://gitee.com/thin-wind/Study/tree/UNIX
* 一个简易的RTSP服务器: https://gitee.com/thin-wind/Module/tree/rtspServer
* Linux网络编程之文件服务器: https://gitee.com/thin-wind/FileServer
* Qt实现的翻金币小游戏: https://gitee.com/thin-wind/qt-coin-game
* Qt实现的俄罗斯方块小游戏: https://gitee.com/thin-wind/qt-cube-game
* Qt实现的串口助手工具: https://gitee.com/thin-wind/qt-serial

### 汽车专题
* UDS诊断协议栈: https://gitee.com/thin-wind/uds
* Python解析DBC，并生成C代码: https://gitee.com/thin-wind/pydbcc
* Python解析arxml文件: https://gitee.com/thin-wind/pyarxml
* AutosarCP CAN传输层规范解读: https://gitee.com/thin-wind/AutosarCP-CanTp
* AutosarCP诊断通信管理规范解读: https://gitee.com/thin-wind/AutosarCP-DCM
* AutosarCP诊断事件管理规范解读: https://gitee.com/thin-wind/AutosarCP-DEM
* OSEK/VDX操作系统(OS)规范解读: https://gitee.com/thin-wind/ISO17356-3
* AutosarCP操作系统规范解读: https://gitee.com/thin-wind/AutosarCP-OS

### 项目
* 海思HI3518E视频编解码传输项目: https://gitee.com/thin-wind/Study/tree/HiMPP
* 基于Django开发的个人网站: https://gitee.com/thin-wind/myweb

### 杂散项
* 一些零散的东西: https://gitee.com/thin-wind/Misc


### [个人学习路线图](https://pan.baidu.com/s/1Tc-UUFCnJPyHaSNr7DNY3Q?pwd=bj54)
* [电路基础（电路/数电/模电）](https://pan.baidu.com/s/178u4O_9bz0P8G0GaPsYr2A?pwd=jr2c)
* [C语言基础](https://pan.baidu.com/s/1K9DBJOCXQVKCa0JEpoYHSg?pwd=der1)
* [51单片机](https://pan.baidu.com/s/1-3AItb5qxr9IN7w5GUpohw?pwd=pvqs)
* [STM32学习](https://pan.baidu.com/s/12wT-wKd-9DpelIpLKLPf1Q?pwd=hrnv)
* [绘制原理图及PCB](https://pan.baidu.com/s/1cdQqm1jwXKyqYAsq4UGPuA?pwd=q4av)
* [Linux基础](https://pan.baidu.com/s/1SU-SxLzkPm2gP0MYiuVnAg?pwd=qfmw)
* [ARM裸机开发](https://pan.baidu.com/s/1oS435vosCsNJhEcElPYb5w?pwd=gzpc)
* [C语言专题精讲](https://pan.baidu.com/s/1Lbrei3nTCqGjxVXgq4gwzQ?pwd=ja4m)
* [C语言数据结构](https://pan.baidu.com/s/1nPTN5x7UlGYas2_7Xpy8_Q?pwd=qdnp)
* [uboot和系统移植](https://pan.baidu.com/s/1dry1_vnJ1s85Q33ISdLlag?pwd=w5q5)
* [Linux应用编程和网络编程](https://pan.baidu.com/s/1UP5c7iBXEi4B-qlf7ArO3A?pwd=uuze)
* [Linux驱动开发](https://pan.baidu.com/s/1kiXaCLhImPElvIVn-5By7Q?pwd=aew6)
* [图片解码播放器](https://pan.baidu.com/s/141yuL1fms5hEVcb-bzebMQ?pwd=k6u2)
* [Python基础及应用编程](https://pan.baidu.com/s/15ZrHQ76gPkP4XrWrYpCBIQ?pwd=1nj3)
* [Web开发](https://pan.baidu.com/s/1tB-0Y33GuxbGsa0tUsUICQ?pwd=89p7)
* [C++核心编程](https://pan.baidu.com/s/1G0IovEuUny9qgrbR5raTxA?pwd=2rjp)
* [Qt基础](https://pan.baidu.com/s/1lY4B9Ltgm30pzy-IwNZCIg?pwd=sg6u)
* [LiteOS](https://pan.baidu.com/s/1eZCgW0O91t-bwArS0Vguxw?pwd=d7u3)
* [LwIP](https://pan.baidu.com/s/1d_-tCX2LspgzK5PisK6Anw?pwd=1r3t)
* [海思HI3518E方案视频编解码传输深度学习](https://pan.baidu.com/s/1gq-ZgpQ1VDZXQzp4f2kW6w?pwd=7sqi)
* [Linux内核精讲](https://pan.baidu.com/s/1I2D3K4Gs0tveYXh2BBNb7g?pwd=r3z2)
* [8086汇编基础](https://pan.baidu.com/s/1-2SmZgzGDY9ReZeJYSzs1w?pwd=neuz)
* [Linux内核0.12完全注释](https://pan.baidu.com/s/1phQmINqBNdvAwqDi1dFD9Q?pwd=qcw5)
* [跟我一起写Makefile](https://pan.baidu.com/s/1igWCpZ0EmVACC4irDW_oxA?pwd=9x4v)
* [深入浅出操作系统](https://pan.baidu.com/s/1IfFGEZFBXmbC46ljaKGExA?pwd=msgr)
* [操作系统真象还原](https://pan.baidu.com/s/1pUsiTkgbqDSU-YqZA2-LzA?pwd=bgmr)
* [Linux网络编程之文件服务器](https://pan.baidu.com/s/1ZJQXSIoKKNqEJQaJVhP4Jg?pwd=882d)
* [FreeRTOS内核实现与应用开发实战](https://pan.baidu.com/s/1t7he_V6d3kyz4hdq2ojnMA?pwd=dt9e)
* [ISO15765-2规范](https://pan.baidu.com/s/1cv_rE2JekGQdi10vXcBbdg?pwd=6rov)
* [AutosarCP-CanTp规范](https://pan.baidu.com/s/1UJ8d5Gu2ucGsvmsBF4snUg?pwd=xrqj)
* [AutosarCP-DCM规范](https://pan.baidu.com/s/1KG_koEGFFKQMq3hR45friA?pwd=wptt)
* [ISO17356-3规范](https://pan.baidu.com/s/1de3sU0zmc8R7xRKRY-GahQ?pwd=9724)
* [LVGL移植与使用](https://pan.baidu.com/s/1atG-IdIspat8KsFDp8WoaA?pwd=e03l)

